@extends('layouts.app')

@section('content')
  <article class="blog-article">
    <header>
      <h3>
        Jordy
      </h3>
    </header>
    <p>
       Ik ben Jordy, maar mensen noemen mij Jord, een persoon die zeer geinteresseerd is in hoe technologie communiceert, user interfaces en natuurlijk programmeren (vaak websites).
       Ik ben een persoon die vaak druk is in zijn doen en laten, maar deze drukte ook ten goede kan benutte als ik iets leuk vind of doe. Natuurlijk kan deze drukte ook nadelig zijn, namelijk dat ik snel afgeleid raak.
     </p>
     <p>
       Voordat ik aan deze HBO studie begon heb ik MBO Applicatie ontwikkelaar afgerond en heb hierbij ruim 1800 uur stage gelopen bij 2 verschillende bedrijven waarbij ik vandaag de dag werk bij één van deze bedrijven.
       Bij dit bedrijf houd ik mij bezig met zowel web als apps en alles wat zich eromheen afspeelt.
     </p>
     <p>
       Ik heb voor deze studie gekozen omdat dit perfect aansluit op mijn MBO studie en omdat er zeker van ben dat ik een carriëre wil als zelfstandig programmeur.
       Zelf ben ik zeer zeker van mijn studiekeuze, al vanaf dat ik begon aan mijn MBO studie keek ik er al naar uit om Informatica te studeren. Ik heb hiervoor al veel moeten leren om het juiste niveau te bereiken, zowel volwassenheid en kundigheid op het gebied van programmeren.
     </p>
     <strong>
       SKC en de opleiding
     </strong>
     <p>
       De SKC was zeer simpel, dit komt omdat ik alle onderwerpen al vaak ben tegengekomen.
       Ook bevestigde de matchings dag en de digitale speurtocht wat ik al wist en dacht over deze opleiding, namelijk dat het zeer goed bij mij past.
     </p>
     <p>
       Ik verwacht van dit studiejaar veel nieuwe leerstof en ook verwacht ik dat bepaalde gaten in mijn huidige kennis worden opgevult.
     </p>
     <strong>
       Mijn studieplek/game omgeving
     </strong>
     <br>
     <img width="800" src="{!! url('/img/werkplek.jpg') !!}"/>
  </article>
@endsection
