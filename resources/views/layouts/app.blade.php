<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="{!! url('/css/app.css') !!}"/>
      <title>SLB Blog Jordy Houwaart</title>
      <script type="text/javascript" src="{!! url('/js/app.js') !!}">

      </script>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

      <!-- Styles -->
  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-inverse bg-inverse fixed-top">
       <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>
       <div class="collapse navbar-collapse" id="navbarsExampleDefault">
         <ul class="navbar-nav mr-auto">
           <li class="nav-item active">
             <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="{!! url('/blog/periode-1') !!}">Periode 1</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="{!! url('/blog/periode-2') !!}">Periode 2</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="{!! url('/blog/periode-3') !!}">Periode 3</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="{!! url('/blog/periode-4') !!}">Periode 4</a>
           </li>
         </ul>
       </div>
     </nav>
     <div id="page-content-wrapper">
       <div id="page-content-container">
            @yield('content')
       </div>
     </div>
  </body>
</html>
