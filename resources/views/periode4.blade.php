@extends('layouts.app')

@section('content')
  <article class="blog-article">
    <header>
      <h3>
          Zit ik op mijn plek?
      </h3>
    </header>
    <br><br><b>Wat vond je van dit studiejaar?</b><br>
    Dit studiejaar is anders verlopen dan ik had verwacht. Ik heb wat tegenslagen gehad waar ik gelukkig bovenop ben gekomen.
    Ook vond ik de verhuizing naar de nieuwe locatie minder goed dan dat er verteld was.
    <br><br><b>Heeft dit studiejaar voldaan aan je verwachtingen?</b><br>
    Ja, het heeft wel voldaan aan mijn verwachtingen. Omdat ik vanaf MBO gekomen ben wist ik dat er een enorm verschil zou zijn in les materiaal en de les methodes.
    <br><br><b>Is Informatica de juiste opleiding voor jou en waarom wel/niet?</b><br>
    Ja, ik heb het reuze naar mijn zin op informatica. Ik kwam hier met het plan om Software Engineer te studeren maar ben daar gelukkig al vrij snel vanaf gestapt omdat media beter aansluit bij mijn ervaringen en interesses.
    <br><br><b>Wat kenmerkt een goede informatica student?</b><br>
    Een goede informatica student is nieuwsgierig, probleemoplossend en vasthoudend.
    <br><br><b>In hoeverre pas jij binnen dit plaatje?</b><br>
    Ik pas redelijk goed in het plaatje.
    Ik hou ervan om problemen op te lossen in een leuke context en om problemen te kunnen oplossen moet je vasthoudend zijn.
    Het laaste is iets wat ik zeker ben. Ik bijt mij vaak vast in iets.
    <br><br><b>Hoe ziet studiejaar 2 er voor jou uit? Welke vakken staan er volgend jaar op de planning? Maak
    een overzicht per periode en voeg deze bij. Denk ook aan herkansingen en keuzevakken!</b><br>
    <img src="{!! url('/img/vakken-media.png')!!}" alt="">
    <br><br><b>Met welke leerdoelen ga je volgend jaar aan de slag?</b><br>
    Mijn leerdoelen voor volgend jaar zijn:
    <ul>
      <li>Nieuwe techonology aanraken en mij eigen maken</li>
      <li>Betere structuur volgen bij projectmatig werken</li>
      <li>Meer ervaring opdoen met het communiceren tussen hardware of applicaties</li>
    </ul>

  </article>
@endsection
