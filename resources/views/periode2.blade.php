@extends('layouts.app')

@section('content')
  <article class="blog-article">
    <header>
      <h3>
        Effectief studeren
      </h3>
    </header>
    <b>
      Wat versta jij onder effectief studeren?
    </b>
    <br />
    Een manier van studeren zoeken die bij jezelf past. Leren op de manier die je zelf het prettigst vindt bespaard veel tijd.
    Effectief studeren is ook het goed indelen van de leerstof over de geplande tijd.
    <br />
    <br />
    <b>
      Tijdens de les ‘Effectief studeren 1’ is er aandacht besteed aan het kijken van videocolleges.
      Hoe ervaar je het kijken van videocolleges? Kijk je ze ook weleens twee keer?
    </b>
    <br />
    Ik kijk videos meestal 2 keer. Ik bekijk de lessen voordat ik naar de les ga en nog een keer voor de toets.

    <br />
    <br />

    <b>
      Tijdens de les ‘Effectief studeren 1’ heb je geoefend met het maken van aantekeningen bij een videocollege. In hoeverre verschilden de gemaakte aantekeningen van elkaar? Voeg een foto bij ter illustratie.
    </b>
    <div class="row">
      <div class="col-md-6">
        Voor:
        <br>
        <img width="400" src="{!! url('/img/mindmap.png') !!}"/>
      </div>
      <div class="col-md-6">
        Na:
        <br>
        <img width="400" src="{!! url('/img/samenvatting.png') !!}"/>
      </div>
    </div>

    <br />
    <br />

    <b>
      Wat kan je zeggen over jouw timemanagement?
    </b>
    <br />
    Time management blijft een lastig iets voor mij. Ik probeer het zo efficient mogelijk in te delen maar het komt regelmatig voor dat het niet loopt zoals geplanned.

    <br />
    <br />

    <b>
      Hoeveel tijd heb je de afgelopen week besteed aan studeren? Is dit te veel, te weinig of precies voldoende volgens jou?
    </b>
    <br />
    Voor mijn gevoel draai ik goed mee op mijn kennis en is er nog geen druk om echt veel te studeren. Dus over afgelopen week kan ik zeggen dat het precies voldoende is.

    <br />
    <br />

    <b>
      Met welke leerdoelen op het gebied van effectief studeren ga je de komende periode aan de slag?
    </b>
    <br />
    Nu ik weet hoe het eraan toe gaat in een tentamenweek is mijn doel om het leren voor de toets beter in te plannen.
    Ik heb de vorige keer veel tijd uitgesteld en het niet goed verdeelt over de tijd die ik had. Dit zorgde ervoor dat het ineens veel informatie was dat ik moest onthouden.
  </article>
@endsection
