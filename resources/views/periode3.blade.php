@extends('layouts.app')

@section('content')
  <article class="blog-article">
    <header>
      <h3>
        Specialisatiekeuze
      </h3>
    </header>
      <b>
        Welke vakken vond je tot nu toe het leukst/het meest interessant en waarom?
      </b><br>
        Tot nu toe vond ik IARCH, IRDB en IWDR het leukst, dit omdat deze vakken het meest bij media horen.
      <br>
      <br>

      <b>
        Welke challengeweek vond je tot nu toe het leuks/het meest interessant en waarom?
      </b><br>
        Van de 2 challengeweeks die ik gehad heb vond ik media het leukst. Het programmeren is leuk, maar het daadwerkelijk in actie zien is het beste wat er is.
      <br>
      <br>
        <b>
          Tijdens de les ‘Specialisatiekeuze’ heb je per specialisatie een wordcloud gemaakt. Welke
          specialisatie heeft jouw voorkeur en waarom? Voeg de wordcloud van deze specialisatie bij.
        </b><br>

        De specialisatie media heeft bij mij de grootste voorkeur, dit omdat ik al enig tijd actief ben in deze richting en ik het nog steeds met veel plezier doe.
        <br>
        <img width="600" src="{!! url('/img/wordcloudp3.png') !!}" alt="">
      <br>
       <br>
      <b>
        Is jouw voorkeur voor deze specialisatie logisch als je kijkt naar jouw favoriete vakken?
      </b><br>
        Ja, al mijn favoriete vakken hebben te maken met media.
       <br>
       <br>
      <b>
        Is jouw voorkeur voor deze specialisatie logisch als je kijkt naar de challengeweek die je het
        leukst vond?
      </b><br>
      Mijn keuze heeft niets te maken met de challenge week, dit omdat robotica niet mijn interesse heeft als het gaat om media.
     <br>
     <br>

      <b>
        Hoe zeker ben je ervan dat je deze specialisatie gaat kiezen?
      </b><br>
        Zo zeker als dat 1 == 1 TRUE returned.
      <br>
      <br>

      <b>
        Welke beroepen zijn er mogelijk binnen deze specialisatie?
      </b><br>
      <ul>
        <li>Front end developer</li>
        <li>Back end developer</li>
        <li>App developed</li>
        <li>Web developer</li>
        <li>Web design</li>
      </ul>
      <br>
      <br>
      <b>
        Welke beroepen spreken jou het meest aan?
      </b><br>
       Web developer en app developer.
       <br>
       <br>
      <b>
        Welke tweede- en derdejaars modules spreken jou aan? (Zie hiervoor het SLB-naslagwerk).
      </b><br>
      IKMEDT en IKPMD.



  </article>
@endsection
