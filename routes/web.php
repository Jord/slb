<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/blog/periode-1', function () {
    return view('periode1');
});

Route::get('/blog/periode-2', function () {
    return view('periode2');
});

Route::get('/blog/periode-3', function () {
    return view('periode3');
});

Route::get('/blog/periode-4', function () {
    return view('periode4');
});
